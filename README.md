# muwebview

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
这个是python的pywebview的使用，可使用自己的接口实践


#### 安装教程

1.  安装扩展：pip install pywebview -i https://pypi.tuna.tsinghua.edu.cn/simple或者pip install pywebview
2.  安装扩展:: pip install pyinstaller
3.  扩展pyinstaller用于打包

#### 使用说明

1.  接口使用：MyApi，前端都可使用
2. 不需要debug在21行设置debug设置成Fase
3. 打包：使用命令pyinstaller --onefile --windowed main.py或者pyinstaller --onefile main.py
4. 打包的dist文件必须将frontend文件夹放入打包文件内

